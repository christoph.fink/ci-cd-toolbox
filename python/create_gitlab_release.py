#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2019 Christoph Fink, University of Helsinki
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 3
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.

"""Creates a new release of the GitLab project."""

import json
import os
import sys

import requests


try:
    API_BASE_URL = "{CI_API_V4_URL:s}/projects/{CI_PROJECT_ID:s}/".format(**os.environ)
    HEADERS = {
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": os.environ["GITLAB_API_TOKEN"]
    }

    with requests.get(
        "{}/packages".format(API_BASE_URL),
        headers=HEADERS,
        params={
            "package_type": "pypi",
            "order_by": "created_at",
            "sort": "desc",
            "per_page": 1
        }
    ) as response:
        package = response.json()[0]
        package_id = package["id"]
        package_name = package["name"]
        package_version = package["version"]
        # tag_name = package["pipelines"][0]["ref"]
        # print(json.dumps(package, indent=4, sort_keys=True))

        del package

    with requests.get(
        "{}/packages/{}/package_files".format(API_BASE_URL, package_id),
        headers=HEADERS
    ) as response:
        package_files = response.json()
        links = [
            {
                "name": package_file["file_name"],
                "url": "{CI_PROJECT_URL:s}/-/package_files/{id:d}/download".format(
                    id=package_file["id"],
                    **os.environ
                ),
                "link_type": "package"
            }
            for package_file in package_files
        ]
        del package_files

    data = {
        "tag_name": os.environ["CI_COMMIT_TAG"],
        "name": "{} {}".format(package_name, package_version),
        "assets": {
            "links": links
        }
    }

    with requests.post(
        "{}/releases".format(API_BASE_URL),
        headers=HEADERS,
        json=data
    ) as response:
        print(json.dumps(response.json(), indent=4, sort_keys=True))

except Exception as exception:  # pylint: disable=broad-except
    print("Uploading release failed: ", exception, file=sys.stderr)
